=============
How to guides
=============

.. toctree::
   :maxdepth: 1

   configuration.rst
   extending.rst
   redirect_to_login.rst
